import React, { useState } from "react";
import { AddCategory, GifGrid } from "./components/index";

const GifExpertApp = () => {

  const [categories, setCategory] = useState(["Dragon ball"]);

  return (
    <>
      <h2>GifExpeApp</h2>
      <AddCategory 
        categories={categories}
        setCategory={setCategory}
      />
      <hr />

      <ol>
        {categories.map((category, index) => {
          return <GifGrid 
            category={category}
            key={ index }
          />
        })}
      </ol>
    </>
  );
};

export default GifExpertApp;
