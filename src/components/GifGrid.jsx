import { useFetchGifs } from '../hooks/useFetchGifs';
import GifGridItem from './GIfGridItem';

const GifGrid = ({ category }) => {

    const { data: images, loading } = useFetchGifs(category);

    return (
        <>
            <h3>{category}</h3>

            { loading && <p>Cargando</p>}

            <div className="card-grid">
                {
                    images.map(({ id, title, url }) => {
                        return (
                            <GifGridItem
                                key={id}
                                title={title}
                                url={url}
                            />
                        )
                    })
                }
            </div>
        </>
    );
}

export default GifGrid;